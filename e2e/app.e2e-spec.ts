import { ProjectClassPage } from './app.po';

describe('project-class App', function() {
  let page: ProjectClassPage;

  beforeEach(() => {
    page = new ProjectClassPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
