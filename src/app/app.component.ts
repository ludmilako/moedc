import { Component } from '@angular/core';
import { AngularFire } from 'angularfire2';   // ex 7 class

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Test Moed C';

    constructor(af:AngularFire){    // ex 7 class
      console.log(af);
    };
}
