import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Movie} from '../movie/movie';  // ex 6
import {NgForm} from '@angular/forms'; // ex 6

@Component({
  selector: 'hadardasim-movie-form',
  templateUrl: './movie-form.component.html',
  styleUrls: ['./movie-form.component.css']
})
export class MovieFormComponent implements OnInit {
  @Output() movieAddedEvent = new EventEmitter<Movie>();  // ex 6
  movie:Movie = {name:'', rating:'', type:''};  // ex 6

   onSubmitMovie(form:NgForm){  // ex 6
   // console.log('it worked!');   ex 6, to check if the button 'submit' working fine
   console.log(form);
   this.movieAddedEvent.emit(this.movie);
   this.movie = {
     name:'',
     rating:'',
     type:'',
   }
  }

  constructor() { }

  ngOnInit() {
  }

}

