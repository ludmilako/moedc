import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2';  // ex 7 class
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class ProductsService {
 // private _url = 'http://jsonplaceholder.typicode.com/users';    ex 7 class

productsObservable;    /// ex 7 class
  getProducts(){
   /// return this._http.get(this._url).map(res => res.json()).delay(2000); ex 7 class delete this // ex 7 class get the users from firebase
                  
   //this.productsObservable = this.af.database.list('/products');                     
  // return this.productsObservable; 
    
        this.productsObservable = this.af.database.list('/products').map(      /// use this for the join
      products =>{
        products.map(
          product => {
            product.prodCate = [];
            for(var p in product.category){
                product.prodCate.push(
                this.af.database.object('/categorys/' + p)
              )
            }
          }
        );
        return products;
      }
    )
     return this.productsObservable;   
     
 	}
   




  
 // updateUser(user){
  //  let user1 = {name:user.name,email:user.email}  // from the ex with the cancel -> conected to users.component.ts
  //}

  addProduct(product){       // ex 7 class
    this.productsObservable.push(product);
  }
  updateProduct(product){     ///// ex 7 class      after that changes in users.component.ts
    let productKey = product.$key;
    let productData = {pname:product.pname, cost:product.cost};
    this.af.database.object('/products/' + productKey).update(productData);
  }
  deleteProduct(product){                      //// ex 7 class     from users.component.ts
    let productKey = product.$key;
    this.af.database.object('/products/' + productKey).remove();
  }
  constructor(private af:AngularFire) { }   ////   private _http:Http  is was like that in ex 6 and changed in ex 7 class

}
