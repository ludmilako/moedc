import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule,Routes } from '@angular/router';  // ex 6 class
import { AngularFireModule} from 'angularfire2';   /// ex 7 class

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DemoComponent } from './demo/demo.component';
import { PostsComponent } from './posts/posts.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { PostsService } from './posts/posts.service';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { PostFormComponent } from './post-form/post-form.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './product/product.component';
import { ProductsService } from './products/products.service';
import { MovieFormComponent } from './movie-form/movie-form.component';
import { MoviesComponent } from './movies/movies.component';
import { MoviesService } from './movies/movies.service';
import { MovieComponent } from './movie/movie.component';




const appRoutes:Routes = [    // ex 6 class
  //{path:'users',component:UsersComponent},
  //{path:'posts',component:PostsComponent},
  //{path:'products',component:ProductsComponent},//// example test
  {path:'movies',component:MoviesComponent},
  {path:'movie-form',component:MovieFormComponent},
  {path:'',component:MoviesComponent}, 
  {path:'',component:MovieFormComponent},
  {path:'**',component:PageNotFoundComponent},
]


  // Initialize Firebase  ex 7 class
  export const firebaseConfig = {
     apiKey: "AIzaSyChiAB6YNL1K9x42i8kQxQnlOsHC-ZX0Ns",
    authDomain: "moedc-b691e.firebaseapp.com",
    databaseURL: "https://moedc-b691e.firebaseio.com",
    projectId: "moedc-b691e",
    storageBucket: "moedc-b691e.appspot.com",
    messagingSenderId: "87464220409"
  };



@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    PostsComponent,
    UserComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    UserFormComponent,
    PostFormComponent,
    ProductsComponent,
    ProductComponent,
    MovieFormComponent,
    MoviesComponent,
    MovieComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,  // ex 6 class
    RouterModule.forRoot(appRoutes),  // ex 6 class
    AngularFireModule.initializeApp(firebaseConfig)   /// ex 7 class
  ],
  providers: [UsersService, PostsService, ProductsService, MoviesService],
  bootstrap: [AppComponent]
  
  
})
export class AppModule { }
