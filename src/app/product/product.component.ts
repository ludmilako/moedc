import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {Product} from './product';

@Component({
  selector: 'hadardasim-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  inputs:['product']
})
export class ProductComponent implements OnInit {
  product:Product;
  tempProduct:Product = {pid:null,cost:null,pname:null,categoryId:null};  // for ex 5        
  @Output() deleteEvent = new EventEmitter<Product>();
  @Output() cancelEvent = new EventEmitter<Product>(); // for ex 5 []
  isEdit:Boolean = false;
  editButtonText = "Edit";
  constructor() { }
  sendDelete(){
    this.deleteEvent.emit(this.product);
  }
  toggelEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = "save" : this.editButtonText = "Edit";

         if(this.isEdit){   /// for ex 5          
       this.tempProduct.cost = this.product.cost;
       this.tempProduct.pname = this.product.pname;
     } else {
      // let originalAndNew = [];
      // originalAndNew.push(this.tempUser,this.user);
       this.cancelEvent.emit(this.product);  // need explanation ####
     } 

  }
  
  onCancel(){ // for ex 5
    this.isEdit = false;
    this.product.cost = this.tempProduct.cost;
    this.product.pname = this.tempProduct.pname;
    this.editButtonText = 'Edit';
  }
  ngOnInit() {
  }

}
