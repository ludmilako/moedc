import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {User} from '../user/user';  // ex 6 class
import {NgForm} from '@angular/forms'; // ex 6 class

@Component({
  selector: 'hadardasim-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  @Output() userAddedEvent = new EventEmitter<User>();  // ex 6 class
  user:User = {name:'', email:''};  // ex 6 class

   onSubmit(form:NgForm){  // ex 6 class
   // console.log('it worked!');   ex 6 class, to check if the button 'submit' working fine
   console.log(form);
   this.userAddedEvent.emit(this.user);
   this.user = {
     name:'',
     email:''
   }
  }

  constructor() { }

  ngOnInit() {
  }

}
