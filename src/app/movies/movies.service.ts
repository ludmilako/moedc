import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2'; /// ex 7
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class MoviesService {
 // private _url = 'http://jsonplaceholder.typicode.com/posts';    ex 7

 moviesObservable;   // ex 7 
  getMovies(){
 //   return this._http.get(this._url).map(res => res.json()).delay(2000);   ex 7 // ex 7 class get the posts from firebase
      
   this.moviesObservable = this.af.database.list('/movies').delay(3000);                   
   return this.moviesObservable;     
  }
 /*           this.postsObservable = this.af.database.list('/posts').map(      /// use this for the join
      posts =>{
        posts.map(
          post => {
            post.userNames = [];
            for(var p in post.users){
                post.userNames.push(
                this.af.database.object('/users/' + p)
              )
            }
          }
        );
        return posts;
      }
    )
     return this.postsObservable;
 	}   */
  
    addMovie(movie){       // ex 7 class
    this.moviesObservable.push(movie);
  }
    updateMovie(movie){     ///// ex 7       after that changes in posts.component.ts
    let movieKey = movie.$key;
    let movieData = {name:movie.name, rating:movie.rating, type:movie.type};
    this.af.database.object('/movies/' + movieKey).update(movieData);
  }
    deleteMovie(movie){                      //// ex 7     from posts.component.ts
    let movieKey = movie.$key;
    this.af.database.object('/movies/' + movieKey).remove();
  }
  constructor(private af:AngularFire) { }    // it was private _http:Http before ex 7 

}
