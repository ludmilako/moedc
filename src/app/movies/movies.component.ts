import { Component, OnInit } from '@angular/core';
import { MoviesService } from './movies.service';

@Component({
  selector: 'hadardasim-movies',
  templateUrl: './movies.component.html',
  styles: [`
    .movies li { cursor: default; }
    .movies li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover 
    .list-group-item.active:focus {  
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class MoviesComponent implements OnInit {

  isLoading:Boolean = true;
  movies;

  currentMovie;

  select(movie){
    this.currentMovie = movie;
  }
  constructor(private _moviesService:MoviesService) { }

 // addPost(post){  //ex 6        delete it in ex 7
 // this.posts.push(post)
 // }
  addMovie(movie){                         /// ex 7     create this + the one in service for add a new post to firebase
    this._moviesService.addMovie(movie);
  }
  updateMovie(movie){                              // ex 7     beafore that there were changes in posts.service.ts
    this._moviesService.updateMovie(movie);          // until now 2 changes to create updatepost
  }
  /*          delete this in ex 7
  deletePost(post){
    this.posts.splice(
      this.posts.indexOf(post),1
    )
  }
  */
    deleteMovie(movie){                             /// create this in ex 7  and after that posts.service.ts
      this._moviesService.deleteMovie(movie);
    }
  /*   delete that in ex 7 class when updating a post
  cancel(post){  // for ex 5    
    //this._usersService.updateUser(user); why to use 'usersService' ??? same code like users.component.ts
    let post1 = {title:post.title,body:post.body,author:post.author};
    console.log(this.posts);
  }
  */

  ngOnInit() {
    this._moviesService.getMovies().subscribe(moviesData =>
    {this.movies = moviesData;
    this.isLoading = false
    console.log(this.movies)}); // ex 7
  }

}
