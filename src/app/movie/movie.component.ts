import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {Movie} from './movie';

@Component({
  selector: 'hadardasim-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css'],
  inputs:['movie']
})
export class MovieComponent implements OnInit {
  movie:Movie;
  tempMovie:Movie = {name:null,rating:null,type:null};  // for ex 5
  @Output() deleteEvent = new EventEmitter<Movie>();
  @Output() cancelEvent = new EventEmitter<Movie>();  // for ex 5
  isEdit:Boolean = false;
  editButtonText = "Edit";

  constructor() { }

  sendDelete(){
    this.deleteEvent.emit(this.movie);
  }
  toggleEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = "Save" : this.editButtonText = "edit"

             if(this.isEdit){   /// for ex 5     
       this.tempMovie.name = this.movie.name;
       this.tempMovie.rating = this.movie.rating;
        this.tempMovie.type = this.movie.type;
      // this.tempPost.author = this.post.author;
     } else {     
       this.cancelEvent.emit(this.movie);   // need explanation ####
     }
  }
    onCancel(){ // for ex 5
    this.isEdit = false;
    this.movie.name = this.tempMovie.name;
    this.movie.rating = this.tempMovie.rating;
     this.movie.type = this.tempMovie.type;
  //  this.post.author = this.tempPost.author;
    this.editButtonText = 'Edit';
  }

  ngOnInit() {
  }

}
