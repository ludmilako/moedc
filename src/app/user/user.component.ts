import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {User} from './user';

@Component({
  selector: 'hadardasim-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']
})
export class UserComponent implements OnInit {
  user:User;
  tempUser:User = {email:null,name:null};  // for ex 5        
  @Output() deleteEvent = new EventEmitter<User>();
  @Output() cancelEvent = new EventEmitter<User>(); // for ex 5 []
  isEdit:Boolean = false;
  editButtonText = "Edit";
  constructor() { }
  sendDelete(){
    this.deleteEvent.emit(this.user);
  }
  toggelEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = "save" : this.editButtonText = "Edit";

         if(this.isEdit){   /// for ex 5          
       this.tempUser.email = this.user.email;
       this.tempUser.name = this.user.name;
     } else {
      // let originalAndNew = [];
      // originalAndNew.push(this.tempUser,this.user);
       this.cancelEvent.emit(this.user);  // need explanation ####
     } 

  }
  
  onCancel(){ // for ex 5
    this.isEdit = false;
    this.user.email = this.tempUser.email;
    this.user.name = this.tempUser.name;
    this.editButtonText = 'Edit';
  }
  ngOnInit() {
  }

}
